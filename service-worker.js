// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//var dataCacheName = 'weatherData-v1'; (you can create a separate cache to store only data. So if there are changes to the UI the data isn't affected. 
//Here we are manually generating the list of files to cache. So everytime these are updated we must update the cacheName. This isn't ideal. Workbox handles
//this more gracefully by integrating it into your build process. Only changed files will be updated saving bandwidth for users and easier to maintain for you!
const cacheName = 'elevateLosCabos-v1'; //cache to open and store cached files
const filesToCache = [
 //appshell files & resources
  '/', //root
  '/index.html',
  '/scripts/main.js', //the main app functionality which determines what is updated on network connection and what gets stored in cache
  '/scripts/install.js', //to install pwa
  '/styles/inline.css', //CSS styles
  '/images/abc.svg', //add necessary paths to images to cache for offline load
  
];

console.log('Hello from service-worker.js');

self.addEventListener('install', function(e) { 
/*'self' is "window" in js allowing access to any property of window. 
* addEventListener is a built-in function in JavaScript which takes the event to listen for, and a second argument to be called when the 
* event gets fired. - syntax: element.addEventListener(event, listener);  Where 'event' is any valid JavaScript event. 'listner' can
*be a JavaScript function that responds when the event occurs.
*/
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      //This opens the cache (and creates it if not already created) and provides a cache name. Cache name allows us to version files or separate 
      //data from the cached resources so we can easily update one but not affet the other. 
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
  self.skipWaiting(); //allows the updated service worker to take control immediately
});

self.addEventListener('activate', function(e) { //activate event cleans up any old data in the cache
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  self.clients.claim(); //allows updated service worker to take control immediately. Without this and the skipWaiting function the old service worker would
  //continue to control the page as long as there is a tabl open to the page. 
});

self.addEventListener('fetch', function(e) {
  console.log('[Service Worker] Fetch', e.request.url);
  if (e.request.mode !== 'navigate') {
    //Not a page navigtion, bail.
    return;
  }
    e.respondWith(
      fetch(e.request)
      .catch(() => {
        return caches.open(cacheName)
        .then((cache) => {
          return cache.match('offline.html');
        }); 
        /* The fetch handler only needs to handle page nav so other requests can be dumped out of the handler and dealt with normally by the browser. 
        *but if the request .mode is navigate, use fetch to try to get the item from the network. If it fails, catch handler opens the cache with 
        *caches.open(cachName) and uses cache.match('offline.html') to get the precached offline page. The result is sent back to the browser using the 
        * e.respondWith(). Wrapping the fetch call in e.respondWith() prevents the browsers default fetch handling and tells the browser we want to handle
        * the response ourselves. If you don't call e.respondWith() inside of a fetch handler, you'll just get the default network behavior. 
        */

      })    
    );
  });
  