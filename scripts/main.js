// check that service worker is registered
if ('serviceWorker' in navigator) {
	//Use the window load event to keep the page load performant
	window.addEventListener('load', () => {
		navigator.serviceWorker.register('/service-worker.js');
	}) .then(function() {
		console.log('Service Worker Registered');

	}) .catch(function(error)) {
		console.log('Service Worker registration failed', error);
	};
}